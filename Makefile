.POSIX:

include config.mk

OBJECTS = main.o

all: options vw

options:
	@echo vw build options:
	@echo "CFLAGS  = $(MYCFLAGS)"
	@echo "LDFLAGS = $(MYLDFLAGS)"
	@echo "CC      = $(CC)"

.c.o:
	$(CC) $(MYCFLAGS) -c $<

vw: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(MYLDFLAGS)

# Remove binary and object files
clean:
	rm -f vw $(OBJECTS)

# Install program
install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f vw $(DESTDIR)$(PREFIX)/bin
	chmod +x $(DESTDIR)$(PREFIX)/bin/vw
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/vw

.PHONY: all options clean install uninstall
