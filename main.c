#define _POSIX_C_SOURCE 200809L // POSIX.1-2008

#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#define PROGNAME "vw"
#define MIN(a, b)		((a) < (b) ? (a) : (b))
#define MAX(a, b)		((a) < (b) ? (b) : (a))

/* Logic:
 * 1. Get term size
 * 2. Find required amount of lines.
 * 3. Read each line until cutoff
 */

int main(int argc, char **argv) {
	int status = 0, ret = 0, fd = 0;
	size_t buf_len = 0, width = 0, height = 0;
	uint8_t *buf = NULL;

	// Check arguments
	if (argc != 2) {
		fprintf(stderr, "Invalid usage\n");
		fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		return 1;
	}

	// Open file
	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Failed to open '%s': %s\n", argv[0], strerror(errno));
		status = 1; goto err_file;
	}

	// Memory map file
	{
		// Get file size for use with mmap
		struct stat statbuf = {0};
		ret = fstat(fd, &statbuf);
		if (ret != 0) {
			fprintf(stderr, "Failed to get filesize of '%s': %s\n", argv[0],
					strerror(errno));
			status = 1; goto err_mmap;
		}

		// mmap file
		buf = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
		if (buf == NULL) {
			fprintf(stderr, "Failed to mmap '%s': %s\n", argv[0], strerror(errno));
			status = 1; goto err_mmap;
		}
		buf_len = (size_t)statbuf.st_size;
	}

	// Get term size
	{
		const char *columns_s = NULL, *lines_s = NULL;
		struct winsize size = {0};

		// Use env LINES and COLUMNS and if they are NULL use ioctl.
		columns_s = getenv("COLUMNS");
		if (columns_s != NULL) {
			ret = sscanf(columns_s, "%zu", &width);
			if (ret != 1) {
				fprintf(stderr, "Failed to parse COLUMNS of '%s': %s\n",
				        columns_s, strerror(errno));
				status = 1; goto err_term;
			}
		}
		lines_s = getenv("LINES");
		if (lines_s != NULL) {
			ret = sscanf(lines_s, "%zu", &height);
			if (ret != 1) {
				fprintf(stderr, "Failed to parse LINES of '%s': %s\n",
				        lines_s, strerror(errno));
				status = 1; goto err_term;
			}
		}

		// Use ioctl to find term size
		if (width == 0 || height == 0) {
			ret = ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
			if (ret != 0) {
				fprintf(stderr, "ioctl failed: %s\n", strerror(errno));
				status = 1; goto err_term;
			}
		}
		if (width == 0) {
			width = size.ws_col;
		}
		if (height == 0) {
			height = size.ws_row;
		}
	}

	// Print each line with line wrapping
	{
		size_t i = 0, n = 0, lines = 0;
		while (i < buf_len) {
			uint8_t ch = buf[i];
			if (ch == '\n') {
				n = 0;
				lines++;
			}

			if (n >= width) {
				fputc('\n', stdout);
				n = 0;
				lines++;
			}
			if (lines >= height) {
				ret = getchar();
				if (ret == EOF) {
					fputc('\n', stdout);
					break;
				}
				lines = 0;
			}
			fputc(buf[i], stdout);

			i++; n++;
		}
	}

err_term:
	munmap(buf, buf_len);
err_mmap:
	close(fd);
err_file:
	return status;
}
